/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import {
  buyItem,
  createGame,
  getAllMessages,
  getAvailableShopItems,
  solveMessage
} from './service/requests'
import Utils from './utils'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    game: null,
    messages: null,
    shopItems: null,
    networkRequest: false,
    solvedMessage: null,
    boughtItems: [],
    notification: ''
  },
  mutations: {
    startGame (state, payload) {
      state.boughtItems = []
      state.solvedMessage = null
      state.messages = null
      state.game = payload
    },

    updateGame (state, payload) {
      state.game = {...state.game, ...payload}
    },
    updateMessages (state, payload) {
      state.messages = Utils.processMessages(payload)
    },
    updateShopItems (state, payload) {
      state.shopItems = payload
    },
    notify (state, payload) {
      state.notification = payload
      setTimeout(() => {
        state.notification = ''
      }, 2000)
    },
    updateBoughtItems (state, payload) {
      if (!state.boughtItems.includes(payload)) {
        state.boughtItems.push(payload)
      }
    }
  },
  actions: {
    startGame () {
      this.state.networkRequest = true
      createGame().then((res) => {
        this.commit('startGame', res)
        if (res.gameId) {
          getAllMessages(res.gameId).then((res) => {
            this.commit('updateMessages', res)
            this.state.networkRequest = false
          })
          getAvailableShopItems(res.gameId).then((res) => {
            this.commit('updateShopItems', res)
          })
        }
      })
    },

    clickOnAd (context, payload) {
      const adId = payload.adId
      const gameId = this.state.game.gameId
      this.state.networkRequest = true

      solveMessage(gameId, adId).then((res) => {
        if (!res.error) {
          this.commit('updateGame', res)
          this.state.solvedMessage = {adId: adId, result: (res.success) ? 'SUCCESS!' : 'FAIL!'}
        } else {
          this.commit('notify', 'No ad by this ID exists! Try another task...')
        }

        getAllMessages(gameId).then((res) => {
          this.state.networkRequest = false
          setTimeout(() => {
            this.commit('updateMessages', res)
            this.state.solvedMessage = null
          }, 800)
        }).catch((err) => {
          this.state.networkRequest = false
          this.$store.state.solvedMessage = null
        })
      }).catch((err) => {
        this.state.networkRequest = false
        this.commit('notify', 'Server had some problems, try again!')
      })
    },

    buyShoppingItem (context, payload) {
      const itemId = payload.id
      const gameId = this.state.game.gameId
      if (payload.cost && payload.cost <= this.state.game.gold) {
        buyItem(gameId, itemId).then((res) => {
          this.state.networkRequest = true
          this.commit('notify', `You bought ${payload.name} for ${payload.cost}€.`)
          this.commit('updateBoughtItems', payload.name)

          this.commit('updateGame', res)
          getAvailableShopItems(gameId).then((res) => {
            this.commit('updateShopItems', res)
            this.state.networkRequest = false
          })
        })
      }
    }
  },
  getters: {
  }
})
