import {TasksRiskValues} from './config/RiskValues'

export default class Utils {
  static getProbability (probabilityText) {
    const probability = TasksRiskValues[probabilityText];
    return (probability) || 0.8;
  }

  static b64DecodeUnicode (str) {
    try {
      return decodeURIComponent(atob(str).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
      }).join(''))
    } catch (e) {
      return 'Unable to decode the message...';
    }
  }

  static processMessages (messages) {
    const sortedMessages = [];
    messages.map((msg) => {
      const message = {...msg};
      if (message.encrypted) {
        message.message = this.b64DecodeUnicode(message.message)
        message.probability = this.b64DecodeUnicode(message.probability)
      }
      message.ev = message.reward * (1 - this.getProbability(message.probability))
      sortedMessages.push(message);
    });

    return sortedMessages;
  }
}
