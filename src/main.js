import Vue from 'vue'
import Notifications from 'vue-notification'
import store from './store'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'

Vue.config.productionTip = false
Vue.use(Notifications)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
