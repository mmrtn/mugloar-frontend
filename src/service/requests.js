import request from './index'

export function createGame () {
  return request('https://www.dragonsofmugloar.com/api/v2/game/start', {method: 'POST'})
    .then((data) => {
      return data
    })
}

export function getAllMessages (gameID) {
  return request(`https://www.dragonsofmugloar.com/api/v2/${gameID}/messages`)
    .then((data) => {
      return data
    })
}

export function solveMessage (gameID, adId) {
  return request(`https://www.dragonsofmugloar.com/api/v2/${gameID}/solve/${adId}`, {method: 'POST'})
    .then((data) => {
      return data
    })
}

export function getAvailableShopItems (gameID) {
  return request(`https://www.dragonsofmugloar.com/api/v2/${gameID}/shop`)
    .then((data) => {
      return data
    })
}

export function buyItem (gameID, itemId) {
  return request(`https://www.dragonsofmugloar.com/api/v2/${gameID}/shop/buy/${itemId}`, {method: 'POST'})
    .then((data) => {
      return data
    })
}
