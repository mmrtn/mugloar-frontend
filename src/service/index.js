export default function request (baseUrl, options = {method: 'GET'}) {
  const url = baseUrl
  return fetch(url, options)
    .then(res => {
      if (res.ok) {
        return res.json()
      } else {
        if (res.status === 400) {
          console.error(res)
          return res.json()
        }
        throw Error(`Request rejected with status ${res.status}`)
      }
    })
    .catch(error => {
      console.error('Fetch Error', error)
      console.log(error)
    })
}
