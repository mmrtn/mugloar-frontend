export const TasksRiskValues = {
  'Piece of cake': 0.1,
  'Sure thing': 0.15,
  'Walk in the park': 0.25,
  'Quite likely': 0.33,
  'Hmmm....': 0.5,
  'Gamble': 0.51,
  'Rather detrimental': 0.55,
  'Playing with fire': 0.66,
  'Suicide mission': 0.9,
  'Impossible': 0.99
}
